import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import { GlobalStyles } from "./globalStyles";

ReactDOM.render(
  <React.StrictMode>
    <>
      <GlobalStyles />
      <App />
    </>
  </React.StrictMode>,
  document.getElementById("root")
);
