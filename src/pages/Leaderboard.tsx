import { useState, useEffect } from "react";
import { useIndexedDB } from "react-indexed-db";
import { useHistory } from "react-router";
import { Table } from "../components";
import { Container } from "../components";
import { Button } from "../components";
import { LeaderboardData } from "../types";

const Leaderboard = () => {
  const [data, setData] = useState<LeaderboardData[]>([]);
  const history = useHistory();
  const { getAll } = useIndexedDB("leaderboard");

  useEffect(() => {
    (async () => {
      const leaderboardData = await getAll<LeaderboardData>();
      leaderboardData.sort((a, b) => b.score - a.score);
      setData(leaderboardData);
    })();
    // eslint-disable-next-line
  }, []);

  const navigateToWelcome = () => {
    history.push("/");
  };

  const navigateToPlay = () => {
    history.push("/game");
  };

  return (
    <Container direction="column">
      <h2 style={{ textAlign: "center" }}>SCOREBOARD</h2>
      <Table>
        <Table.Row>
          <Table.Cell isTitle text="Rank" />
          <Table.Cell isTitle text="Name" />
          <Table.Cell isTitle text="Date" />
          <Table.Cell isTitle text="Score" />
        </Table.Row>
        {data.map((item, index) => (
          <Table.Row key={item.id}>
            <Table.Cell isTitle text={(index + 1).toString()} />
            <Table.Cell text={item.playerName} />
            <Table.Cell text={item.date} />
            <Table.Cell text={item.score.toString()} />
          </Table.Row>
        ))}
        <Container
          justify="space-between"
          align="center"
          style={{ width: "50%", marginTop: 50 }}
        >
          <Button onClick={navigateToWelcome}>To Welcome Screen</Button>
          <Button onClick={navigateToPlay}>PLAY!</Button>
        </Container>
      </Table>
    </Container>
  );
};

export default Leaderboard;
