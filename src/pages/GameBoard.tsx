import { useHistory } from "react-router";
import { useIndexedDB } from "react-indexed-db";
import Container from "../components/Container";
import { GameContainer } from "../containers";
import { getCurrentDate, PlayerHandler } from "../lib";

const GameBoard = () => {
  const playerName = PlayerHandler.getPlayerName();
  const { add } = useIndexedDB("leaderboard");
  const history = useHistory();

  const onGameTimeFinished = (score: number) => {
    add({
      playerName,
      score,
      date: getCurrentDate(),
    });
    history.push("/leaderboard");
  };

  return (
    <Container direction="column" justify="center" align="center">
      <GameContainer onTimeFinished={onGameTimeFinished} />
    </Container>
  );
};

export default GameBoard;
