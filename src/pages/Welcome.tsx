import { useHistory } from "react-router";
import { Container } from "../components";
import { PlayerContainer } from "../containers";
import { PlayerHandler } from "../lib";

const Welcome = () => {
  const history = useHistory();
  const playOnClick = (playerName: string) => {
    PlayerHandler.setPlayerName(playerName);
    history.push("/game");
  };

  return (
    <Container expanded justify="center">
      <PlayerContainer playOnClick={playOnClick} />
    </Container>
  );
};

export default Welcome;
