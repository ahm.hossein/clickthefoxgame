export { default as WelcomePage } from "./Welcome";
export { default as GameBoardPage } from "./GameBoard";
export { default as LeaderboardPage } from "./Leaderboard";
