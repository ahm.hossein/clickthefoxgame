import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { WelcomePage, GameBoardPage, LeaderboardPage } from "../pages";

const AppNavigation = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={WelcomePage} />
      <Route exact path="/game" component={GameBoardPage} />
      <Route exact path="/leaderboard" component={LeaderboardPage} />
    </Switch>
  </Router>
);

export default AppNavigation;
