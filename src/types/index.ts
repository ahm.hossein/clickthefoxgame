import { ReactElement } from "react";

export type GeneralProps = {
  children: ReactElement | ReactElement[];
};

export type Dog = {
  message: string;
};

export type Cat = {
  url: string;
};

export type Fox = {
  image: string;
};

export type Pets = {
  url: string;
  type: "fox" | "dog" | "cat";
};

export type LeaderboardData = {
  id: number;
  playerName: string;
  date: string;
  score: number;
};
