import { initDB } from "react-indexed-db";
import { Header } from "./components";
import AppNavigation from "./routes";
import config from "./DBConfig";

initDB(config);

function App() {
  return (
    <div className="App">
      <Header>
        <Header.Title text="Click The Fox! Game" />
      </Header>
      <AppNavigation />
    </div>
  );
}

export default App;
