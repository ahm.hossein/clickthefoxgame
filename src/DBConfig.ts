import { IndexedDBProps } from "react-indexed-db";

const config: IndexedDBProps = {
  name: "CTFDB",
  version: 1,
  objectStoresMeta: [
    {
      store: "leaderboard",
      storeConfig: { keyPath: "id", autoIncrement: true },
      storeSchema: [
        {
          name: "playerName",
          keypath: "playerName",
          options: { unique: false },
        },
        {
          name: "date",
          keypath: "date",
          options: { unique: false },
        },
        {
          name: "score",
          keypath: "score",
          options: { unique: false },
        },
      ],
    },
  ],
};

export default config;
