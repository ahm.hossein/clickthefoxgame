import styled from "styled-components";

export const Tile = styled.img`
  width: 200px;
  height: 280px;
  object-fit: cover;
  padding: 0;
  margin: 0;
  flex-basis: 33.3%;
  cursor: pointer;
`;
