import { GeneralProps } from "../../types";
import Container from "../Container";
import { Tile } from "./styles";

type PetProps = React.ComponentProps<typeof Tile>;

const Game = ({ children, ...props }: GeneralProps) => (
  <Container
    direction="column"
    justify="center"
    align="center"
    style={{ margin: 0, padding: 0 }}
    {...props}
  >
    {children}
  </Container>
);

const Row = ({ children, ...props }: GeneralProps) => (
  <Container style={{ padding: 0, margin: 0, flexWrap: "wrap" }} {...props}>
    {children}
  </Container>
);

const Pet = ({ ...props }: PetProps) => <Tile alt="Game Tile" {...props} />;

Game.Pet = Pet;
Game.Row = Row;

export default Game;
