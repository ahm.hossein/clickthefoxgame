import Container from "../Container";
import Loader from "react-loader-spinner";
import { GeneralProps } from "../../types";

const Loading = ({ children, ...restProps }: GeneralProps) => (
  <Container justify="center" align="center" {...restProps}>
    {children}
  </Container>
);

const PuffLoading = () => (
  <Loader type="Puff" color="#f80" height={100} width={100} />
);

Loading.PuffLoading = PuffLoading;

export default Loading;
