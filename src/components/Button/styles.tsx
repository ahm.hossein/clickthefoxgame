import styled from "styled-components";

export const Btn = styled.button`
  border-radius: 6px;
  background-color: #f80;
  color: white;
  border: 0;
  font-size: 20px;
  cursor: pointer;
  min-width: 250px;
  height: 60px;
  :disabled {
    background-color: transparent;
    border: 1px solid #f80;
    color: #000;
  }
`;
