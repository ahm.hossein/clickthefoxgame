import { Btn } from "./styles";

type Props = React.ComponentProps<typeof Btn>;

const Button = ({ children, ...restProps }: Props) => (
  <Btn {...restProps}>{children}</Btn>
);

export default Button;
