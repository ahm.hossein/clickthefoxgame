import { GeneralProps } from "../../types";
import { Container, Title } from "./styles";

type HeaderProps = React.ComponentProps<typeof Container> & GeneralProps;

type TitleProps = { text: string };

const Header = ({ children, ...props }: HeaderProps) => (
  <Container {...props}>{children}</Container>
);

const HeaderTitle = ({ text, ...props }: TitleProps) => (
  <Title {...props}>{text}</Title>
);

Header.Title = HeaderTitle;

export default Header;
