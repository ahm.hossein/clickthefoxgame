import React from "react";
import { GeneralProps } from "../../types";
import Container from "../Container";
import { Caption, Input } from "./styles";
import Button from "../Button";

type CaptionProps = {
  text: string;
} & React.ComponentProps<typeof Caption>;

type NameInputProps = React.ComponentProps<typeof Input>;

type InputChangeProps = React.ChangeEvent<HTMLInputElement>;

type PlayButtonProps = React.ComponentProps<typeof Button>;

const Player = ({ children, ...props }: GeneralProps) => (
  <Container justify="space-between" direction="column" {...props}>
    {children}
  </Container>
);

const Row = ({ children, ...props }: GeneralProps) => (
  <Container align="center" {...props}>
    {children}
  </Container>
);

const PlayerCaption = ({ text, ...props }: CaptionProps) => (
  <Caption {...props}>{text}</Caption>
);

const PlayerNameInput = ({ onChange, ...props }: NameInputProps) => {
  const onChangeText = (event: InputChangeProps) => {
    onChange(event.target.value);
  };
  return <Input onChange={onChangeText} {...props} />;
};

const Play = ({ ...props }: PlayButtonProps) => (
  <Button {...props}>Play</Button>
);

Player.Row = Row;
Player.Caption = PlayerCaption;
Player.NameInput = PlayerNameInput;
Player.PlayButton = Play;

export default Player;
