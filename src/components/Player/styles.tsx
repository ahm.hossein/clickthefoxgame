import styled from "styled-components";

export const Caption = styled.span`
  font-size: 18px;
  margin-right: 8px;
`;

export const Input = styled.input`
  display: inline-block;
  border: 1px solid #f80;
  padding: 12px;
  border-radius: 6px;
`;
