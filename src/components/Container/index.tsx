import { GeneralProps } from "../../types";
import { Row, Column, ContainerViewProps } from "./styles";

type ContainerProps = {
  direction?: "column" | "row";
} & GeneralProps &
  React.ComponentProps<typeof Row> &
  ContainerViewProps;

const Container = ({
  direction = "row",
  expanded = false,
  justify = "flex-start",
  align = "stretch",
  children,
  ...props
}: ContainerProps) => {
  const Component = direction === "column" ? Column : Row;
  return (
    <Component align={align} justify={justify} expanded={expanded} {...props}>
      {children}
    </Component>
  );
};

export default Container;
