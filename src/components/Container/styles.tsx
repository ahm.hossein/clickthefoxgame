import styled from "styled-components";

export type ContainerViewProps = {
  expanded?: boolean;
  justify?: string;
  align?: string;
};

export const Column = styled.div`
  display: flex;
  flex-direction: column;
  padding: 8px;
  height: ${({ expanded }: ContainerViewProps) => (expanded ? "80%" : "auto")};
  justify-content: ${({ justify }: ContainerViewProps) => justify};
  align-items: ${({ align }: ContainerViewProps) => align};
`;

export const Row = styled.div`
  display: flex;
  padding: 8px;
  height: ${({ expanded }: ContainerViewProps) => (expanded ? "80%" : "auto")};
  justify-content: ${({ justify }: ContainerViewProps) => justify};
  align-items: ${({ align }: ContainerViewProps) => align};
`;
