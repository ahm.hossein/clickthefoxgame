import { GeneralProps } from "../../types";
import { TableContainer, TableCell, TableRow } from "./styles";

type CellProps = { isTitle?: boolean; text: string };

type TableProps = React.ComponentProps<typeof TableContainer>;

const Table = ({ children, ...restProps }: TableProps) => (
  <TableContainer {...restProps}>{children}</TableContainer>
);

const Row = ({ children, ...restProps }: GeneralProps) => (
  <TableRow {...restProps}>{children}</TableRow>
);

const Cell = ({ isTitle = false, text, ...restProps }: CellProps) => (
  <TableCell isTitle={isTitle} {...restProps}>
    {text}
  </TableCell>
);

Table.Row = Row;
Table.Cell = Cell;

export default Table;
