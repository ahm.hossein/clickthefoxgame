import styled from "styled-components";

type TableCellProps = {
  isTitle: boolean;
};

export const TableContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const TableRow = styled.div`
  display: flex;
  width: 60%;
  align-items: center;
  border-bottom: 1px solid #eee;
`;

export const TableCell = styled.div`
  flex-basis: 40%;
  &:first-child {
    flex-basis: 10%;
  }
  &:last-child {
    flex-basis: 10%;
    border: 0;
    border-right: 1px solid #eee;
  }
  padding: 8px;
  border-right: 1px solid #eee;
  display: flex;
  justify-content: center;
  font-weight: ${({ isTitle }: TableCellProps) =>
    isTitle ? "bold" : "normal"};
  background-color: ${({ isTitle }: TableCellProps) =>
    isTitle ? "#ccc" : "fff"};
`;
