import { useState, useEffect } from "react";
import { Game, Loading, Container } from "../components";
import { fetchPets } from "../lib/api";
import { makePetsArray } from "../lib/helpers";
import { Pets } from "../types";

type GameContainerProps = {
  onTimeFinished: (score: number) => void;
};

const GameContainer = ({ onTimeFinished }: GameContainerProps) => {
  const [score, setScore] = useState(0);
  const [loading, setLoading] = useState(true);
  const [pets, setPets] = useState<Pets[]>([]);
  const [timeLeft, setTimeLeft] = useState(30);

  useEffect(() => {
    const makeRequest = async () => {
      setLoading(true);
      const pets = await fetchPets();
      setPets(makePetsArray(pets));
      setLoading(false);
    };
    makeRequest();
  }, [score]);

  useEffect(() => {
    if (!timeLeft) {
      onTimeFinished(score);
      return;
    }
    if (loading) return;
    const intervalId = setInterval(() => {
      setTimeLeft((t) => t - 1);
    }, 1000);
    return () => clearInterval(intervalId);
    // eslint-disable-next-line
  }, [timeLeft, loading]);

  const onPetClick = (pet: any) => {
    if (pet.type === "fox") {
      setScore((s) => s + 1);
    } else {
      setTimeLeft((t) => t - 1);
    }
  };

  return (
    <>
      <Container align="center" justify="space-between" style={{ width: 200 }}>
        <div>Score: {score}</div>
        <div>Time Left: {timeLeft}</div>
      </Container>
      {loading ? (
        <Loading>
          <Loading.PuffLoading />
        </Loading>
      ) : (
        <Game>
          <Game.Row>
            {pets.map((pet, index) => {
              return (
                <Game.Pet
                  onClick={() => onPetClick(pet)}
                  key={index.toString()}
                  src={pet.url}
                />
              );
            })}
          </Game.Row>
        </Game>
      )}
    </>
  );
};

export default GameContainer;
