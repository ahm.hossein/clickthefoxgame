import { useState } from "react";
import { Player } from "../components";
import { PlayerHandler } from "../lib";

type Props = {
  playOnClick: (playerName: string) => void;
};

const PlayerContainer = ({ playOnClick }: Props) => {
  const player = PlayerHandler.getPlayerName();
  const [playerName, setPlayerName] = useState(player);
  const [isEditMode, setisEditMode] = useState(!player);

  const nameOnClick = () => {
    setisEditMode((val) => !val);
  };

  return (
    <Player>
      <Player.Row>
        <Player.Caption text="Name: " />
        {isEditMode ? (
          <Player.NameInput value={playerName} onChange={setPlayerName} />
        ) : (
          <Player.Caption onClick={nameOnClick} text={player} />
        )}
      </Player.Row>
      <Player.PlayButton
        onClick={() => playOnClick(playerName)}
        disabled={!playerName.length}
      />
    </Player>
  );
};

export default PlayerContainer;
