export default class Storage {
  static getItem(key: string, defaultValue: string) {
    return localStorage.getItem(key) || defaultValue;
  }

  static setItem(key: string, value: string) {
    localStorage.setItem(key, value);
    return true;
  }

  static removeItem(key: string) {
    localStorage.removeItem(key);
    return true;
  }
}
