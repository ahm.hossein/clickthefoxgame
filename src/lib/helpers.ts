import { Pets } from "../types";

export const shuffle = <T>(array: Array<T>) => {
  var currentIndex = array.length,
    randomIndex;

  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex],
      array[currentIndex],
    ];
  }

  return array;
};

export const makePetsArray = (pets: any): Pets[] => {
  const arr = [];
  for (let i = 0; i < 4; i++) {
    arr.push({ url: pets.cat, type: "cat" });
    arr.push({ url: pets.dog, type: "dog" });
  }
  arr.push({ url: pets.fox, type: "fox" });
  return shuffle(arr) as Pets[];
};

export const getCurrentDate = () => {
  const date = new Date();
  const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  return `${date.getFullYear()}, ${
    monthNames[date.getMonth()]
  } ${date.getDate()}`;
};
