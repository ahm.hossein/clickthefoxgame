import Storage from "./storage";

const PLAYER_NAME_STORAGE_KEY = "player_name";

export default class Player {
  static setPlayerName(playerName: string) {
    Storage.setItem(PLAYER_NAME_STORAGE_KEY, playerName);
  }

  static getPlayerName(): string {
    return Storage.getItem(PLAYER_NAME_STORAGE_KEY, "");
  }
}
