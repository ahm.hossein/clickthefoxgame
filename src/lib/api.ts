import { Cat, Dog, Fox } from "../types";

type PetType = "dog" | "cat" | "fox";

const urls: Record<PetType, string> = {
  dog: "https://dog.ceo/api/breeds/image/random",
  cat: "https://api.thecatapi.com/v1/images/search",
  fox: "https://randomfox.ca/floof/",
};

export const getAPetImage = async <T>(
  petType: PetType
): Promise<T | undefined> => {
  try {
    const url = urls[petType];
    const res = await fetch(url).then((resp) => resp.json());
    return res;
  } catch (e) {
    alert(e.message);
  }
};

export const fetchADog = async () => {
  return await getAPetImage<Dog>("dog");
};

export const fetchACat = async () => {
  return await getAPetImage<Cat[]>("cat");
};

export const fetchAFox = async () => {
  return await getAPetImage<Fox>("fox");
};

export const fetchPets = async () => {
  const res = await Promise.all([fetchADog(), fetchACat(), fetchAFox()]);
  const dog = res[0];
  const cat = res[1];
  const fox = res[2];

  return { dog: dog?.message, cat: cat?.[0].url, fox: fox?.image };
};
